const app = require("./app")

const dotenv = require('dotenv').config({ path: './config.env' });
const mongoose = require('mongoose')

// This should be on top to caught exception
process.on('uncaughtException', err => {
    console.log("uncaughtException");
    process.exit(1);
});

// This should be on top to caught exception
process.on('unhandledRejection', err => {
    console.log("unhandledRejection", err);
    server.close(() => {
        process.exit(1);
    });
});

mongoose.Promise = global.Promise;  // this line eliminate -- (node:92486) DeprecationWarning: Mongoose: mpromise (mongoose's default promise library) is deprecated,

mongoose.connect(process.env.DATABASE, {
    useMongoClient:true,
}).then(() => {
    console.log("DB Connection Successful!");
});

const port = process.env.PORT || 3210;

const server = app.listen(port, () => {
    console.log(`App running on port ${port}`);
});