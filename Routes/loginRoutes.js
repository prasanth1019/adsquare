const express = require("express");
const authController = require("../controller/authController")


const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerOptions = {
    definition: {
      openapi: '3.0.0',
      info: {
        title: 'adSquare api documents',
        version: '1.0.0',
        description: 'adSquare api documents',
        contact: {
            name: "Prasanth Ramanathan"
        },
        servers: ["http://localhost:3210/api/v1"],
      },
    },
    apis: ['aap.js'], // files containing annotations as above
  };
const swaggerDocs = swaggerJsdoc(swaggerOptions);

const router = express.Router();
router.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/**
 * @openapi
 * /:
 *   post:
 *     description: user login!
 *     responses:
 *       200:
 *         description: return success if valid user.
 */
router.route("/login").post(authController.login);

router.route("/logout").post(authController.logout);
router.route("/signUp").post(authController.signUp);
router.route("/userProfile").post(authController.userProfile);
router.route("/getAllProfile").get(authController.protectRoute, authController.userProfiles);
router.route("/forgetPassword").post(authController.forgetPassword);
router.route("/resetPassword/:token").patch(authController.resetPassword);

module.exports = router;