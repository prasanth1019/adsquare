const express = require("express");
const helmet = require("helmet");
const hpp = require('hpp');
const rateLimit = require("express-rate-limit");
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const compression = require('compression')
const loginRoutes = require("./Routes/loginRoutes");
const AppError = require("./utils/appError");
const globalErrorHandler = require("./controller/errorController");

const app = express();

// Helmet helps you secure your Express apps by setting various HTTP headers. It's not a silver bullet, but it can help!
app.use(helmet());

// Use to limit repeated requests to public APIs and/or endpoints such as password reset.
const limiter = rateLimit({
    windowMs: 60 * 60 * 1000, // 15 minutes
    max: 500, // limit each IP to 100 requests per windowMs
    message: "Too many request created from this IP, please try again after an hour!",
});

app.use(hpp());

app.use('/api', limiter);
app.use(express.json( {limit: '10kb'} ));

// Data sanitization against nosql query injection
app.use(mongoSanitize());

// Data sanitization against XSS
app.use(xss());

// Prevent parameter pollution -- prevent query param duplicate error! 
app.use(hpp());

// compression 
app.use(compression());

// Serving satic files!
app.use(express.static(`${__dirname}/public`))

app.use(globalErrorHandler);
app.use('/api/v1', loginRoutes);

app.all('*', (req, res, next) => {
    res.status(404).json({
        status: 'failed',
        message : `can\'t find ${req.originalUrl} on this server`
    });
});

module.exports = app;