const crypto = require("crypto");
var jwt = require('jsonwebtoken');
const { promisify } = require('util');
const User = require("../model/user");
const AppError = require('../utils/appError');
const catchAsync = require("../utils/catchAsync");
const sendEmail = require("../utils/email");


const signJWTToken = (bodyParam) => {

    const token = jwt.sign({ 
        id: bodyParam._id, 
        username: bodyParam.username, 
        email: bodyParam.email, 
        contactNo: bodyParam.contactNo 
    }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });

    return token;
}

const createSendToken = (user, statusCode, res) => {
    const token = signJWTToken(user);
    const cookieOptions = {
        expires: new Date(
            Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
        ),
        httpOnly: true
    }

    if (process.on.NODE_ENV === 'production') cookieOptions.secure = true; 

    res.cookie('jwt', token, cookieOptions);
    
    // Remove password from output.
    user.password = undefined;

    res.status(statusCode).json({
        status: 'success',
        data: {
            user
        }
    });
}

exports.signUp = catchAsync(async (req, res, next) => {
        
        const signUp = await User.create({
            "username": req.body.username,
            "password": req.body.password,
            "email": req.body.email,
            "passwordConfirm": req.body.passwordConfirm,
            "contactNo": req.body.contactNo,
            "passwordChangedAt": req.body.passwordChangedAt,
            "profileId": req.body.profileId,
            "createdDate":  req.body.createdDate,
            "lastLogin":  req.body.lastLogin,
            "emoji":  req.body.emoji,
            "info":  req.body.info,
            "deviceToken":  req.body.deviceToken,
            "role":  req.body.role
        });

        // const token = signJWTToken(signUp);
        
        // res.status(200).json({
        //     status: 'success',
        //     token: token,
        //     data: {
        //         user: signUp
        //     }
        // });

        createSendToken(signUp, 200, res);
});

exports.login = catchAsync(async (req, res, next) => {
    // const signUp = await User.create({
    //     "username": req.body.username,
        
    //     "passwordConfirm": req.body.passwordConfirm,
    //     "contactNo": req.body.contactNo,
    // });
    const { password, email } = req.body;

    // check email & password exist.
    if (!email || !password) {
        return next(new AppError('Please provide email or password', 400))
    }

    // check user exist && password is correct
        
    const user = await User.findOne({email}).select('+password');

    if (!(user) || !(await user.comparePassword(password, user.password))) {
        return next(new AppError('Incorrect email or password', 401))
    }
    
    // If everything ok, send token to client.
    // const token = signJWTToken(user)

    // res.status(200).json({
    //     status: 'success',
    //     token,
    // });
    createSendToken(user, 200, res);

});

exports.logout = catchAsync(async (req, res, next) => {
    
    res.cookie('jwt', 'loggedout', {
        expires: new Date (Date.now() * 10 * 1000),
        httpOnly: true
    });
    
    res.status(200).json({ 'status': 'success' });
});

exports.forgetPassword = catchAsync(async (req, res, next) => {
    
    const {registeredEmailAddress} = req.body;

    // check email address exist.
    if (!registeredEmailAddress) {
        return next(new AppError('Please provide valid email address', 400))
    }
    
    // check valid email address or not.
    const validmailAddressUser = await User.findOne({email: registeredEmailAddress});

    if (!validmailAddressUser) {
        return next(new AppError('Incorrect email address, does not exist!', 401));
    }
    // Generate random reset token
    const resetToken = validmailAddressUser.createPasswordResetToken();

    // if {validationBeforeSave: false} is not used then password confirm error will araise bcoz of using save().
    await validmailAddressUser.save({ validateBeforeSave: false });

    // If everything ok, send it to user email.
    const resetURL = `${req.protocol}://${req.get('host')}/api/v1/resetPassword/${resetToken}`;
    const message = `Forget your password? Submit a PATCH request with your password and Confirm password to ${resetURL}.
     If you did not forget your password, please ignore this email.`;

    try {
        await sendEmail({
            email: validmailAddressUser.email,
            subject: "Your password reset token (valid for 10 min)",
            message
            });
    } catch (err) {
            validmailAddressUser.passwordResetToken = undefined;
            validmailAddressUser.passwordResetExpires = undefined;
            await validmailAddressUser.save({ validateBeforeSave: false });
            console.log(err);
            return next(new AppError('There was an error sending the mail. Try again later! ', 500));
    }

    res.status(200).json({
        status: 'success',
        message: 'Token send to mail'
    });

});

exports.resetPassword = catchAsync(async (req, res, next) => {

        const hashedToken = crypto.createHash('sha256').update(req.params.token).digest('hex');

        const user = await User.findOne({passwordResetToken: hashedToken, passwordResetExpires: {$gt: Date.now()} });

        if (!user) {
            return next(new AppError('Token is invalid or has expired', 400))
        }

        user.password = req.body.password;
        user.passwordConfirm = req.body.passwordConfirm;
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        await user.save();

        // const token = signJWTToken(user)

        // res.status(200).json({
        //     status: 'success',
        //     token,
        // });
        createSendToken(user, 200, res);
});

exports.userProfile = catchAsync(async (req, res, next) => {
    const { username, email } = req.body;
    
    // check email & password exist.
    if (!email && !username) {
        return next(new AppError('Please provide email', 400));
    }

    // const selectedUser = await User.findOne({email, username});
    const selectedUser = await User.findOne({ $or: [ {email},{username} ]});

    // If user not exist 
    if (!selectedUser) {
        return next(new AppError('User not exists', 500));
    }

    res.status(200).json({
        status: 'success',
        selectedUser,
    });

});

exports.userProfiles = catchAsync(async (req, res, next) => {
    
    // {} - means which will select all the users in db.
    const allUsers = await User.find({});

    // If user not exist. 
    if (!allUsers) {
        return next(new AppError('No user exists', 500));
    }

    // send response now.
    res.status(200).json({
        status: 'success',
        allUsers,
    });

});

exports.protectRoute = async (req, res, next) => {
    
    let token = '';
    
    // check if token is available
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1]
    }

    if (!token) {
        return next(new AppError('You are not logged in! Please log in to get access.', 401));
    }

    // validate token
    const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
    console.log(decode);

    // Check if user still exists
    const freshUser = await User.findById(decode.id);

    if (!freshUser) {
        return next(new AppError('The user belonging to this token does not exist.', 401));
    }

    // Check if user changed password after the token was issued.
    if ( await freshUser.changedPasswordAfter(decode.iat)) {
        return next(new AppError('User recently changed password! Please log in again.', 401))
    }

    // grand access to protected route.
    req.user = freshUser;
    next();
}