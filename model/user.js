const crypto = require("crypto");
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require("bcryptjs");

 const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "User name should not be empty"],
        unique: true
    },

    password: {
        type: String,
        required: [true, "please provide a password"],
        minlength: 8,
        select: false
    },

    passwordConfirm: {
        type: String,
        required: [true, "please confirm your password"],
        minlength: 8,
        validate: {
            // This will only work on CREATE and SAVE
            validator: function (el){ 
                return el === this.password;
            },
            message: 'Password are not the same!'
        }
    },

    email: {
        type: String,
        required: [true, "Email should not be empty"],
        unique: true,
        lowercase: true,
        validate: [{ isAsync:false, validator: value => validator.isEmail(value), message: 'Please provide a valid email' }]
    },
    
    contactNo :{
        type: String,
        unique: true
    },
    
    passwordChangedAt:  {
        type: Date,
        default: Date.now()
    },

    passwordResetToken: {
        type: String
    },

    passwordResetExpires: {
        type: Date
    },

    profileId: {
        type: String,
        default: ''
    },

    createdDate: {
        type: Date,
        default: Date.now()
    },

    lastLogin: {
        type: Date,
        default: Date.now()
    },

    emoji: {
        type: String,
        default: '@'
    },

    info: {
        type: String,
        default: 'New user added now'
    },

    deviceToken: {
        type: String
    },

    role: {
        type: String,
        enum: ['user', 'admin', 'supervisor'],
        default: 'user'
    }

 });

 userSchema.pre('save', async function (next) {
     // Only run this fn if this.password actually modified
     if (!this.isModified('password')) return next();
    // hash the password with cost of 12  
     this.password = await bcrypt.hash(this.password, 12);
     // this will eliminate field from db
     this.passwordConfirm = undefined;
     next();
 });

 userSchema.pre('save', async function (next) {
     if ( !this.isModified('password') || this.isNew ) return next();

     this.passwordChangedAt = Date.now() - 1000;
     next();
 });

//  instance method 
userSchema.methods.comparePassword = async function (candidatePassword, userPassword) {
    return await bcrypt.compare(candidatePassword, userPassword);
} 

userSchema.methods.changedPasswordAfter = function(JWTTimestamp) {
    if (this.passwordChangedAt) {
        const changedTimeStamp = parseInt(this.passwordChangedAt.getTime() / 1000, 10);
        return JWTTimestamp < changedTimeStamp;
    }
}

userSchema.methods.createPasswordResetToken = function () {
    const resetToken = crypto.randomBytes(32).toString('hex');
    console.log({resetToken}, this.passwordResetToken);    
    this.passwordResetToken = crypto.createHash('sha256').update(resetToken).digest('hex');
    this.passwordResetExpires = Date.now() + 10 * 60 * 1000;
    return resetToken;
}

const User = mongoose.model('User', userSchema);

module.exports = User;